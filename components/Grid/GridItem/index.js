import React, { memo, useMemo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const styles = makeStyles({
  grid: {
    padding: '0 10px 0 10px',
    // '@media (min-width: 600px)': {
    //   padding: '0 16px 0 16px',
    // },
  },
  itemContainer: {
    padding: '0 !important',
  },
  noPadding: {
    padding: '0 !important',
  },
});

const GridItem = (props) => {
  const classes = styles();
  const { children, className, isContainer, noPadding, ...rest } = props;

  const gridClasses = classNames({
    [classes.grid]: isContainer ? false : true,
    [classes.itemContainer]: isContainer,
    [classes.noPadding]: noPadding,
    [className]: className !== undefined,
  });

  const GridComponent = useMemo(
    () => (
      <Grid item {...rest} container={isContainer ? true : false} className={gridClasses}>
        {children}
      </Grid>
    ),
    [children, gridClasses, isContainer, rest],
  );

  return GridComponent;
};

GridItem.propTypes = {
  className: PropTypes.string,
  isContainer: PropTypes.bool,
  children: PropTypes.node,
  noPadding: PropTypes.bool,
};

export default memo(GridItem);
