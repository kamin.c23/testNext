import React, { memo, useMemo } from 'react';
import PropTypes from 'prop-types';

// @material-ui/core components
import Grid from '@material-ui/core/Grid';

const GridContainer = (props) => {
  const { children, className, ...rest } = props;

  const GridComponent = useMemo(
    () => (
      <Grid container {...rest} className={className}>
        {children}
      </Grid>
    ),
    [children, className, rest],
  );

  return GridComponent;
};

GridContainer.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};

export default memo(GridContainer);
