import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import * as utils from '../../utils';
const Authenticated = (props) => {
  const Layout = props.layout;

  // Component Did Mount
  useEffect(() => {
    const accessToken = utils.auth.loadAuth();
    if (accessToken) {
      // verifyToken().then(result => {
      //   if (!result) {
      //     history.push(process.env.REACT_APP_PUBLIC_URL)
      //   }
      // })
    } else {
      // history.push(process.env.REACT_APP_PUBLIC_URL)
    }
  });

  return (
    <Route path={props.path}>
      <Layout menuItems={props.menuItems} component={props.component} />
    </Route>
  );
};

const mapStateToProps = (state) => ({
  menuItems: state.menu,
});

export default connect(mapStateToProps)(Authenticated);
