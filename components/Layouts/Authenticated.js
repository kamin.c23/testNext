import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import * as utils from '../../utils';
import { useSelector } from 'react-redux';
import DashboardLayout from '../DashboardLayout';

const Authenticated = (props) => {
  // Component Did Mount
  const menuItems = useSelector((state) => state.menu);
  useEffect(() => {
    const accessToken = utils.auth.loadAuth();

    if (accessToken) {
      // verifyToken().then(result => {
      //   if (!result) {
      //     history.push(process.env.REACT_APP_PUBLIC_URL)
      //   }
      // })
    } else {
      // history.push(process.env.REACT_APP_PUBLIC_URL)
    }
  });

  // return <div className="layout1">{props.children}</div>;
  return (
    <DashboardLayout menuItems={menuItems} component={props.children}>
      {props.children}
    </DashboardLayout>
  );
};

export default Authenticated;
