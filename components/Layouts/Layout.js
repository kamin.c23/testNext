import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import Authenticated from '../Layouts/Authenticated';
import DashboardLayout from '../DashboardLayout';

// Pages

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

const Layout = (_props) => {
  return <div />;
};

export default withRouter(Layout);
