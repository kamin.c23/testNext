import React, { useEffect, useState } from 'react';
import { Table } from 'antd';
import 'antd/dist/antd.css';
export default function DataTable(prop) {
  const [pageSize, setPageSize] = useState(prop.pageSize ? prop.pageSize : 10);
  const [sortedInfo, setSortedInfo] = useState({ columnKey: '', order: '' });
  const pagenationOption = {
    pageSize: pageSize,
    showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items`,
    pageSizeOptions: ['10', '15', '20', '25', '30', '50', '100'],
    showSizeChanger: true,
  };
  const pagination = prop.paginationEnable === false ? false : pagenationOption;
  const handleChange = (pagination, filters, sorter) => {
    setPageSize(pagination.pageSize);
    setSortedInfo(sorter);
  };

  return (
    <div>
      <Table
        columns={prop.columns}
        rowKey={(record) => record.key}
        dataSource={prop.datas}
        pagination={pagination}
        loading={prop.loading}
        onChange={handleChange}
        scroll={prop.scroll}
        expandable={prop.expandable}
        showHeader={prop.showHeader}
        size={prop.size}
        onRow={(record, rowIndex) => {}}
        // onCell={(record, rowIndex) => {
        //   return {
        //     onClick: (event) => {
        //       console.log('record ', record, rowIndex, event);

        //       if (prop.onRowsClick) {
        //         prop.onRowsClick();
        //       }
        //     },
        //   };
        // }}
        // onRow={(record, rowIndex) => {
        //   return {
        //     onClick: (event) => {
        //       if (prop.onRowsClick) {
        //         // prop.onRowsClick();
        //       }
        //     },
        //   };
        // }}
        // className={'cell-low-height'}

        //   rowClassName={(record) =>
        //   }
      />
    </div>
  );
}
