import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';

import { GridContainer, GridItem } from 'components/Grid';
import Hero from 'components/Hero';

const AuthTemplate = ({ children }) => {
  const classes = useStyles();
  return (
    <Hero type={'primary'}>
      <div className={classes.container}>
        <GridContainer>
          <Hidden smDown>
            <GridItem xs={12} sm={12} md={6} lg={6}>
              <div className={classes.imgContainer}>
                <img
                  src="/images/login/login-person.png"
                  alt="CP Future Campus"
                  height="500"
                  width="500"
                  className={classes.imgElementPerson}
                />
                <img
                  src="/images/login/doodle-1.png"
                  alt="CP Future Campus"
                  height="150"
                  width="150"
                  className={classes.imgElement1}
                />
                <img
                  src="/images/login/doodle-2.png"
                  alt="CP Future Campus"
                  height="180"
                  width="100"
                  className={classes.imgElement2}
                />
                <img
                  src="/images/login/doodle-3.png"
                  alt="CP Future Campus"
                  height="80"
                  width="80"
                  className={classes.imgElement3}
                />
                <img
                  src="/images/login/doodle-4.png"
                  alt="CP Future Campus"
                  height="80"
                  width="80"
                  className={classes.imgElement4}
                />
              </div>
            </GridItem>
          </Hidden>
          <GridItem xs={12} sm={12} md={6} lg={6}>
            <div className={classes.card}>
              <div className={classes.cardContainer}>{children}</div>
            </div>
          </GridItem>
        </GridContainer>
      </div>
    </Hero>
  );
};

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: `calc(100vh - ${theme.footer.height} - ${theme.header.height})`,
    padding: '10px 0',
    [theme.breakpoints.down('sm')]: {
      minHeight: `80vh`,
    },
  },
  card: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: '#FFF',
    borderRadius: '10px',
    boxShadow: '0px 1px 1px rgba(0, 0, 0, 0.25);',
    padding: '20px',
  },
  cardContainer: {
    width: '100%',
    textAlign: 'center',
    '& > h1': {
      color: '#4F4F4F',
      fontWeight: '500',
    },
    '& > p': {
      color: '#828282',
      fontWeight: 'normal',
    },
  },
  imgContainer: {
    position: 'relative',
    minHeight: '400px',
    width: '100%',
    height: '100%',
  },
  imgElementPerson: {
    position: 'absolute',
    left: '7%',
    top: '-70px',
    zIndex: '1',
  },
  imgElement1: {
    position: 'absolute',
    right: '20px',
    top: '-30px',
    zIndex: '2',
  },
  imgElement2: {
    position: 'absolute',
    right: '0',
    bottom: '10%',
    zIndex: '2',
  },
  imgElement3: {
    position: 'absolute',
    left: '25%',
    top: '20%',
    zIndex: '2',
  },
  imgElement4: {
    position: 'absolute',
    left: '10%',
    top: '0',
    zIndex: '2',
  },
}));

AuthTemplate.propTypes = {
  children: PropTypes.node,
};

export default AuthTemplate;
