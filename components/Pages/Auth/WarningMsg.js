import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

const WarningMsg = ({ message }) => {
  const classes = useStyles();
  return <div className={classes.warningMsg}>{message}</div>;
};

WarningMsg.propTypes = {
  message: PropTypes.string,
};

const useStyles = makeStyles((theme) => ({
  warningMsg: {
    color: `${theme.palette.warning.main}`,
    borderRadius: '5px',
    minHeight: '30px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: '10px 0',
  },
}));

export default WarningMsg;
