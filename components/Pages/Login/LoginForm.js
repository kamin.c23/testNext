import { useForm, Controller } from 'react-hook-form';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import Input from '../../Input';
import WarningMsg from '../Auth/WarningMsg';

const LoginForm = ({ onSubmit, errorMsg }) => {
  const classes = useStyles();
  const { control, handleSubmit } = useForm();

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      {errorMsg && <WarningMsg message={errorMsg} />}
      <Controller
        name="email"
        control={control}
        defaultValue=""
        render={({ field: { onChange, value }, fieldState: { error } }) => (
          <Input
            id="email"
            labelText={'email'}
            variant="outlined"
            isDense={true}
            isFullWidth={true}
            value={value}
            onChange={onChange}
            isError={!!error}
            helperText={error ? error.message : null}
            autoComplete="off"
          />
        )}
      />
      <Controller
        name="password"
        control={control}
        defaultValue=""
        render={({ field: { onChange, value }, fieldState: { error } }) => (
          <Input
            id="password"
            labelText={'password'}
            variant="outlined"
            isDense={true}
            isFullWidth={true}
            value={value}
            onChange={onChange}
            isError={!!error}
            helperText={error ? error.message : null}
            type="password"
          />
        )}
      />
      <Button
        variant="contained"
        color="primary"
        fullWidth={true}
        className={classes.submitButton}
        type="submit"
      >
        {'loginButton'}
      </Button>
    </form>
  );
};

const useStyles = makeStyles((theme) => ({
  submitButton: {
    margin: '10px 0px',
  },
}));

export default LoginForm;
