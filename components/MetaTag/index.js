import Head from 'next/head';

const Meta = ({ title, keywords, description }) => {
  return (
    <Head>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="keywords" content={keywords} />
      <meta name="description" content={description} />
      <meta charSet="utf-8" />
      <link rel="icon" href="/favicon.ico" />
      <title>{`${title} | CP University`}</title>
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
      />
      <link rel="stylesheet" type="text/css" href="/css/nprogress.css" />
      <link rel="preload" href="/fonts/Kanit/Kanit-Medium.ttf" as="font" crossOrigin="" />
      <link rel="preload" href="/fonts/Kanit/Kanit-Regular.ttf" as="font" crossOrigin="" />
      <link rel="preload" href="/fonts/Kanit/Kanit-SemiBold.ttf" as="font" crossOrigin="" />
      <link rel="preload" href="/fonts/Kanit/Kanit-Light.ttf" as="font" crossOrigin="" />
    </Head>
  );
};

Meta.defaultProps = {
  title: '',
  keywords: 'Learning',
  description: 'Learning with CP University',
};

export default Meta;
