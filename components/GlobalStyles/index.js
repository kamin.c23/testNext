import { createStyles, makeStyles } from '@material-ui/core';

const fontFamilies = [
  '-apple-system',
  'BlinkMacSystemFont',
  'Segoe UI',
  'Roboto',
  'Oxygen',
  'Ubuntu',
  'Cantarell',
  'Fira Sans',
  'Droid Sans',
  'Helvetica Neue',
  'sans-serif',
];

const codeFontFamilies = [
  'source-code-pro',
  'Menlo',
  'Monaco',
  'Consolas',
  'Courier New',
  'monospace',
];

const useStyles = makeStyles(() =>
  createStyles({
    '@global': {
      '*': {
        boxSizing: 'border-box',
        margin: 0,
        padding: 0,
      },
      html: {
        '-webkit-font-smoothing': 'antialiased',
        '-moz-osx-font-smoothing': 'grayscale',
        height: '100%',
        width: '100%',
      },
      body: {
        width: '100% !important',
        height: '100vh',
        margin: '0',
        padding: '0',
        'font-family': fontFamilies.join(','),
      },
      a: {
        textDecoration: 'none',
      },
      '#root': {
        height: '100%',
        width: '100%',
      },
      code: {
        'font-family': codeFontFamilies.join(','),
      },
    },
  }),
);

const GlobalStyles = () => {
  useStyles();

  return null;
};

export default GlobalStyles;
