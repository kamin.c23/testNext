import React from 'react';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const Hero = ({ children, type, maxWidth, style }) => {
  const classes = useStyles();

  const getColor = (type) => {
    if (type === 'primary') return classes.heroPrimary;
    if (type === 'default') return classes.heroDefault;
    if (type === 'dark') return classes.heroDark;
    if (type === 'gray') return classes.heroGray;
  };

  const bgColor = getColor(type);

  return (
    <section className={`${classes.hero} ${bgColor}`} style={style}>
      <Container maxWidth={maxWidth}>{children}</Container>
    </section>
  );
};

Hero.propTypes = {
  children: PropTypes.node,
  type: PropTypes.oneOf(['primary', 'default', 'dark', 'gray']),
  maxWidth: PropTypes.string,
};

Hero.defaultProps = {
  type: 'primary',
  maxWidth: 'lg',
};

const useStyles = makeStyles((theme) => ({
  hero: {
    minHeight: '300px',
  },
  heroPrimary: {
    // background: theme.palette.gradient.main,
    background: '#D4D7DD',
  },
  heroDefault: {
    background: '#FFF',
  },
  heroDark: {
    background: '#000',
  },
  heroGray: {
    background: '#F1F3F4',
  },
}));

export default React.memo(Hero);
