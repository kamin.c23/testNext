import React from 'react';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

const SelectMenu = ({ list, value, label, onChange, placeholder, variantProp }) => {
  const classes = useStyles();
  const variant = variantProp ? variantProp : 'outlined';
  return (
    <FormControl variant={variant} margin={'dense'} style={{ minWidth: '100%' }}>
      <InputLabel id="selectLabel">{label}</InputLabel>
      <Select
        style={{ backgroundColor: 'white' }}
        labelId="selectLabel"
        id="select-main"
        value={value}
        onChange={onChange}
        label={label}
        placeholder={placeholder}
        classes={{ select: classes.selectOverride }}
      >
        {list.map((item) => {
          return (
            <MenuItem key={`menu-${item.id}`} value={item.id}>
              {item.value}
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
};

SelectMenu.propTypes = {
  list: PropTypes.array,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  label: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  variantProp: PropTypes.string,
};

const useStyles = makeStyles((theme) => ({
  selectOverride: {
    '&:focus': {
      backgroundColor: 'transparent',
    },
  },
}));

export default SelectMenu;
