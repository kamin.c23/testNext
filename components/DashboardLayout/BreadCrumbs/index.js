import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import { Col } from 'reactstrap';
import { Avatar } from '@material-ui/core';

// Icons
import { style } from '../../../utils';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  link: {
    display: 'flex',
    alignItems: 'center',
  },
  icon: {
    marginRight: theme.spacing(0.5),
    width: 20,
    height: 20,
  },
  bar: {
    marginBottom: '1rem',
    marginTop: '1rem',
    marginLeft: '2.5rem',
  },
}));

const IconBreadcrumbs = (props) => {
  const classes = useStyles();
  const [items, setItems] = useState([]);
  const [iconNames, setIconNames] = useState([]);

  useEffect(() => {
    let result = [];
    props.menuItems.map((item) => {
      const childs = item.childs;
      if (childs && childs.length > 0) {
        const resultChilds = [];
        childs.map((cItem) => {
          resultChilds.push({
            key: cItem.href.slice(1).replace(/\//g, '.'),
            icon: style.getMaterialIcon(cItem.icon),
            iconImg: cItem.iconImg,
          });
        });
        result.push({
          key: item.href.slice(1),
          icon: style.getMaterialIcon(item.icon),
          iconImg: item.iconImg,
        });
        result = result.concat(resultChilds);
      } else {
        result.push({
          key: item.href.slice(1),
          icon: style.getMaterialIcon(item.icon),
          iconImg: item.iconImg,
        });
      }
    });
    setIconNames(result);
  }, [props.menuItems]);

  useEffect(() => {
    // const paths = location.pathname ? location.pathname.split('/') : [];
    const paths = props.pathName ? props.pathName.split('/') : [];

    let key = [];
    // console.log('path ', location.pathname);
    const result = paths
      .map((p) => {
        if (p) {
          key.push(p);
          return {
            key: key.join('.'),
            text: `${p[0].toUpperCase()}${p.slice(1).replace(/-/g, ' ')}`,
          };
        }
        return null;
      })
      .filter((p) => p);
    setItems([{ text: '', icon: null }].concat(result));
  }, [props.pathName]);

  const renderItems = () => {
    return items.map((item) => {
      let iconItem = iconNames.filter((i) => item.key === i.key);
      if (iconItem[0] && iconItem[0].icon) {
        const IconComponent = iconItem[0].icon;
        return (
          <Typography key={item.key} color="textPrimary" className={classes.link} variant="h4">
            <IconComponent className={clsx(classes.icon)} />
          </Typography>
        );
      } else if (iconItem[0] && iconItem[0].iconImg) {
        return (
          <Typography key={item.key} color="textPrimary" className={classes.link} variant="h4">
            <Avatar className={clsx(classes.icon)}>
              <img alt="icon_image" src={iconItem[0].iconImg} width="16" height="16" />
            </Avatar>
            {item.text}
          </Typography>
        );
      } else if (item.text !== '[id]') {
        return (
          <Typography key={item.key} color="textPrimary" className={classes.link} variant="h4">
            {item.text}
          </Typography>
        );
      } else {
        return <div />;
      }
    });
  };

  return (
    <Col lg="12">
      <Breadcrumbs aria-label="breadcrumb" className={classes.bar}>
        {renderItems()}
      </Breadcrumbs>
    </Col>
  );
};

const mapStateToProps = (state) => ({
  menuItems: state.menu,
});

export default connect(mapStateToProps)(IconBreadcrumbs);
