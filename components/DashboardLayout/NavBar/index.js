import React, { useEffect, useState } from 'react';
import { Link as RouterLink, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Box, Divider, Drawer, Hidden, List, makeStyles, Collapse } from '@material-ui/core';
import NavItem from './NavItem';
// import MiniUserInfo from '../../UserInfo/MiniUserInfo'
import { useRouter } from 'next/router';

const useStyles = makeStyles(() => ({
  mobileDrawer: {
    width: 256,
  },
  desktopDrawer: {
    width: 256,
    top: 64,
    height: 'calc(100% - 64px)',
  },
  avatar: {
    cursor: 'pointer',
    width: 64,
    height: 64,
  },
  box: {
    marginRight: 0,
    marginLeft: 0,
  },
  subMenu: {
    paddingLeft: '30px',
  },
}));

const NavBar = ({ onMobileClose, openMobile, menuItems, userInfo }) => {
  const classes = useStyles();
  // const location = window.location;
  const router = useRouter();

  useEffect(() => {
    if (openMobile && onMobileClose) {
      onMobileClose();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router.pathname]);

  const [open, setOpen] = useState(() => {
    const resultState = {};
    menuItems.map((item) => {
      resultState[item.title] = false;
      return null;
    });
    return resultState;
  });

  const handleClick = (key) => {
    open[key] = !open[key];
    setOpen(Object.assign({}, open));
  };

  const content = (
    <Box height="100%" display="flex" flexDirection="column">
      {/* <MiniUserInfo userInfo={userInfo} /> */}
      <Divider />
      <Box className={classes.box}>
        <List component="nav">
          {menuItems.map((item, mIdx) => {
            if (item.childs && item.childs.length > 0) {
              const subMenus = item.childs;
              return (
                <>
                  <NavItem
                    open={open[item.title]}
                    child={true}
                    key={`menu-${mIdx}-${item.title}`}
                    title={item.title}
                    icon={item.icon}
                    iconImg={item.iconImg}
                    divider={item.divider}
                    onClick={() => handleClick(item.title)}
                  />
                  <Collapse
                    key={`collapse-${item.title}`}
                    in={open[item.title]}
                    timeout="auto"
                    unmountOnExit
                  >
                    {subMenus.map((sItem, sIdx) => {
                      return (
                        <NavItem
                          open={open[item.title]}
                          child={false}
                          key={`submenu-${sIdx}-${item.title}-${sItem.title}`}
                          href={sItem.href}
                          title={sItem.title}
                          icon={sItem.icon}
                          iconImg={sItem.iconImg}
                          divider={sItem.divider}
                        />
                      );
                    })}
                  </Collapse>
                </>
              );
            } else {
              return (
                <NavItem
                  open={open[item.title]}
                  child={null}
                  href={item.href}
                  key={`menu-${mIdx}-${item.title}`}
                  title={item.title}
                  icon={item.icon}
                  iconImg={item.iconImg}
                  divider={item.divider}
                />
              );
            }
          })}
        </List>
      </Box>
      <Box flexGrow={1} />
    </Box>
  );

  return (
    <>
      <Hidden lgUp>
        <Drawer
          anchor="left"
          classes={{ paper: classes.mobileDrawer }}
          onClose={onMobileClose}
          open={openMobile}
          variant="temporary"
        >
          {content}
        </Drawer>
      </Hidden>
      <Hidden mdDown>
        <Drawer anchor="left" classes={{ paper: classes.desktopDrawer }} open variant="persistent">
          {content}
        </Drawer>
      </Hidden>
    </>
  );
};

NavBar.propTypes = {
  onMobileClose: PropTypes.func,
  openMobile: PropTypes.bool,
};

NavBar.defaultProps = {
  onMobileClose: () => {},
  openMobile: false,
};

export default NavBar;
