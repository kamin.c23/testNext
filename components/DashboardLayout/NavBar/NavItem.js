import Link from 'next/link';
import MuiListItem from '@material-ui/core/ListItem';

import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  makeStyles,
  colors,
  ListItemIcon,
  ListItemText,
  ListItemAvatar,
  Avatar,
  withStyles,
} from '@material-ui/core';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import { style } from '../../../utils';
import NavStyle from '../../../components/style/Navbar';
const useStyles = makeStyles((theme) => ({
  item: {
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0,
  },
  button: {
    borderRadius: 0,
    color: theme.palette.text.secondary,
    fontWeight: theme.typography.fontWeightMedium,
    justifyContent: 'flex-start',
    letterSpacing: 0,
    padding: '10px 8px',
    textTransform: 'none',
    width: '100%',
  },
  icon: {
    marginRight: theme.spacing(1),
  },
  title: {
    marginRight: 'auto',
  },
  active: {
    color: colors.common.black,
    backgroundColor: '#FFECC7',
    '& $title': {
      fontWeight: theme.typography.fontWeightMedium,
    },
    '& $icon': {
      color: colors.common.black,
    },
  },
  nested: {
    paddingLeft: '30px',
  },
  avatar: {
    backgroundColor: 'unset',
  },
  // listWrap: {
  //   "&:hover": {
  //     border: "1px solid #6c757d",
  //     color: "black"
  //   },
  //   textAlign: "center",
  //   "&:focus": {
  //     background: "#6c757d",
  //     color: "black"
  //   }
  // }
}));
const ListItem = withStyles({
  root: {
    '&$selected': {
      backgroundColor: '#bcbcbc',
      color: 'white',
      '& .MuiListItemIcon-root': {
        color: 'white',
      },
    },
    '&$selected:hover': {
      backgroundColor: '#7a7777',
      color: 'white',
      '& .MuiListItemIcon-root': {
        color: 'white',
      },
    },
    '&:hover': {
      backgroundColor: '#827e7e',
      color: 'white',
      '& .MuiListItemIcon-root': {
        color: 'white',
      },
    },
  },
  selected: {},
})(MuiListItem);
const NavItem = (props) => {
  const classes = useStyles();
  const IconComponent = style.getMaterialIcon(props.icon);

  const renderExpandIcon = () => {
    if (props.child) {
      return props.open ? <ExpandLess /> : <ExpandMore />;
    } else {
      return <></>;
    }
  };

  const renderMenuItem = () => {
    if (props.child === true) {
      return (
        <>
          <NavStyle />
          <ListItem button activeClassName={classes.active} className={classes.button}>
            {props.icon && (
              <ListItemIcon>
                <IconComponent className={classes.icon} size="20" />
              </ListItemIcon>
            )}
            {props.iconImg && (
              <ListItemAvatar>
                <Avatar className={classes.avatar}>
                  <img alt="icon_image" src={props.iconImg} width="32" height="32" />
                </Avatar>
              </ListItemAvatar>
            )}
            <ListItemText className={classes.title} primary={props.title} />
            {renderExpandIcon()}
          </ListItem>
          {props.divider ? <Divider /> : <></>}
        </>
      );
    } else if (props.child === false) {
      return (
        <>
          <NavStyle />
          <Link href={props.href}>
            <ListItem
              button
              // component={Link}
              to={props.href}
              activeClassName={classes.active}
              className={classes.nested + ' ' + classes.button}
            >
              {props.icon && (
                <ListItemIcon>
                  <IconComponent className={classes.icon} size="20" />
                </ListItemIcon>
              )}
              {props.iconImg && (
                <ListItemAvatar>
                  <Avatar className={classes.avatar}>
                    <img alt="icon_image" src={props.iconImg} height="100%" />
                  </Avatar>
                </ListItemAvatar>
              )}
              <ListItemText className={classes.title} primary={props.title} />
              {renderExpandIcon()}
            </ListItem>
          </Link>
          {props.divider ? <Divider /> : <></>}
        </>
      );
    } else {
      return (
        <>
          <NavStyle />{' '}
          <Link href={props.href}>
            <ListItem
              button
              activeClassName={classes.active}
              className={classes.button}
              // component={Link}
              // selected
              selected={props.href.split('/')[1] === location.pathname.split('/')[1]}
              to={props.href}
            >
              {props.icon && (
                <ListItemIcon>
                  <IconComponent className={classes.icon} size="20" />
                </ListItemIcon>
              )}
              {props.iconImg && (
                <ListItemAvatar>
                  <Avatar className={classes.avatar}>
                    <img alt="icon_image" src={props.iconImg} width="32" height="32" />
                  </Avatar>
                </ListItemAvatar>
              )}
              <ListItemText className={classes.title} primary={props.title} />
            </ListItem>
          </Link>
          {props.divider ? <Divider /> : <></>}
        </>
      );
    }
  };

  return (
    <ListItem className={clsx(classes.item, props.className)} disableGutters {...props}>
      {renderMenuItem()}
    </ListItem>
  );
};

NavItem.propTypes = {
  className: PropTypes.string,
  href: PropTypes.string,
  icon: PropTypes.elementType,
  title: PropTypes.string,
  iconImg: PropTypes.string,
};

NavItem.defaultProps = {
  iconImg: null,
  icon: null,
};

export default NavItem;
