import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core';
import IconBreadcrumbs from './BreadCrumbs';
import { useRouter } from 'next/router';
import dynamic from 'next/dynamic';

const NavBar = dynamic(() => import('./NavBar'), { ssr: false });
const TopBar = dynamic(() => import('./TopBar'), { ssr: false });

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    display: 'flex',
    height: '100%',
    overflow: 'hidden',
    width: '100%',
    // margin:'2rem'
  },
  wrapper: {
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden',
    paddingTop: 64,
    [theme.breakpoints.up('lg')]: {
      paddingLeft: 256,
    },
  },
  contentContainer: {
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden',
    margin: '1rem',
  },
  content: {
    flex: '1 1 auto',
    height: '100%',
    overflow: 'auto',
  },
}));

const DashboardLayout = (props) => {
  const classes = useStyles();
  const [isMobileNavOpen, setMobileNavOpen] = useState(false);
  const router = useRouter();

  const breadCrumbObj = props.menuItems.find((o) => o.href === router.pathname);
  const breadCrumb = breadCrumbObj ? breadCrumbObj.title : router.pathname;
  return (
    <div className={classes.root}>
      <TopBar onMobileNavOpen={() => setMobileNavOpen(true)} />
      <NavBar
        onMobileClose={() => setMobileNavOpen(false)}
        openMobile={isMobileNavOpen}
        {...props}
      />
      <div className={classes.wrapper}>
        <div className={classes.contentContainer}>
          <div className={classes.content}>
            <IconBreadcrumbs pathName={breadCrumb} />
            {props.component}
          </div>
        </div>
      </div>
    </div>
  );
};

export default DashboardLayout;
