import { createStyles, makeStyles } from '@material-ui/core';

const fontFamilies = [
  '-apple-system',
  'BlinkMacSystemFont',
  'Segoe UI',
  'Roboto',
  'Oxygen',
  'Ubuntu',
  'Cantarell',
  'Fira Sans',
  'Droid Sans',
  'Helvetica Neue',
  'sans-serif',
];

const codeFontFamilies = [
  'source-code-pro',
  'Menlo',
  'Monaco',
  'Consolas',
  'Courier New',
  'monospace',
];

const useStyles = makeStyles(() =>
  createStyles({
    '@global': {
      '*': {
        boxSizing: 'border-box',
        margin: 0,
        padding: 0,
      },
      html: {
        '-webkit-font-smoothing': 'antialiased',
        '-moz-osx-font-smoothing': 'grayscale',
        height: '100%',
        width: '100%',
      },
      body: {
        width: '100% !important',
        height: '100vh',
        margin: '0',
        padding: '0',
        background: '#e69138',
        'font-family': fontFamilies.join(','),
      },
      '.MuiTypography-h4': { fontSize: '24px' },
      a: {
        textDecoration: 'none',
      },
      '#root': {
        height: '100%',
        width: '100%',
      },
      code: {
        'font-family': codeFontFamilies.join(','),
      },
      '.MuiOutlinedInput-root': {
        color: 'white ',
        backgroundColor: '#727682 !important',
      },
      '.MuiInputLabel-outlined': {
        color: 'white',
      },
      multilineColor: {
        color: 'red',
      },
      '.MuiOutlinedInput-notchedOutline': {
        'border-color': '#FFFFFF',
      },
      '.MuiOutlinedInput-root:hover .MuiOutlinedInput-notchedOutline': {
        'border-color': '#F65C35',
      },
      '.MuiFormLabel-root.Mui-focused': {
        color: '#FFFFFF',
      },
      '.MuiAlert-icon': {
        color: '#F44336 !important',
      },
      // '.MuiSvgIcon-root': {
      //   color: '#FFFFFF !important',
      // },
      'input:-webkit-autofill': {
        '-webkit-box-shadow': '0 0 0px 1000px #3C4147 inset',
        '-webkit-text-fill-color': 'white !important',
      },
      '.MuiIconButton-root': {
        color: 'white ',
      },
      'button:focus': { outLine: '0px' },
    },
  }),
);

const LoginStyles = () => {
  useStyles();

  return null;
};

export default LoginStyles;
