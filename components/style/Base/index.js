import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    '@global': {
      '.ant-table': {
        fontFamily: "'Roboto','Kanit' !important",
      },
      '.MuiInputBase-input': {
        fontFamily: "'Roboto','Kanit' !important",
        fontWeight: '400 !important',
      },

      '.MuiFormLabel-root': {
        fontFamily: "'Roboto','Kanit' !important",
        fontWeight: '400 !important',
      },
      '.MuiMenuItem-root': {
        fontFamily: "'Roboto','Kanit' !important",
      },
      '.App': {
        fontFamily: "'Roboto','Kanit' !important",
        height: '100vh',
      },
      label: {
        fontFamily: "'Roboto','Kanit' !important",
      },
    },
  }),
);

const BaseStyles = () => {
  useStyles();

  return null;
};

export default BaseStyles;
