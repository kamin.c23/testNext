import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    '@global': {
      '.MuiList-padding': {
        paddingTop: '0px',
      },
      '.MuiDivider-root': {
        display: 'none',
      },
    },
  }),
);

const LayoutStyles = () => {
  useStyles();

  return null;
};

export default LayoutStyles;
