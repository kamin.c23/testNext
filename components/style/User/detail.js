import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    '@global': {
      '.actions': {
        // position: 'absolute',
        visibility: 'hidden',
        fontFamily: "'Roboto','Kanit'",
      },
      '.ant-table-thead > tr > th, .ant-table-tbody > tr > td, .ant-table tfoot > tr > th, .ant-table tfoot > tr > td':
        {
          padding: '4px 16px 4px 16px !important',
        },
      '.MuiTypography-h4': { fontWeight: 'bold !important', fontSize: '24px !important' },

      '.ant-table-row': {
        position: 'relative',
      },
      '.MuiBreadcrumbs-separator': {
        display: 'none',
      },
      '.MuiBreadcrumbs-li': {
        marginLeft: '0.5rem',
      },
      '.ant-table': {
        borderRadius: '10px !important',
        boxShadow: '0px 0px 3px 2px rgb(0 0 0 / 9%)',
      },
      '.ant-table-row:hover .actions': {
        visibility: 'visible',
      },
      '.ant-table-tbody > tr.ant-table-row:hover > td': {
        backgroundColor: '#FFEEEA !important',
      },
      '.ant-table-thead > tr > th': {
        visibility: 'visible',
        backgroundColor: '#F3F0EB !important',
        fontWeight: 'bold !important',
      },

      '.ant-table-title ': {
        padding: '8px 8px !important',
        fontSize: '20px',
      },
      'table tr:nth-child(2n) td': {
        backgroundColor: '#F9F9F9',
      },
      '.modal-title': {
        width: '100%',
      },
      '.close': {
        display: 'none',
      },
      '.modal-topic': { fontSize: '13px', lineHeight: '16px', color: '#757575' },
      '.modal-detail': {
        fontSize: '16px',
        lineHeight: '19px',
        color: '#1D2228',
        fontWeight: '700 !important',
      },
      '.test': { backgroundColor: 'red' },
    },
  }),
);

const UserStyles = () => {
  useStyles();

  return null;
};

export default UserStyles;
