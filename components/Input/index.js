import React, { forwardRef, memo, useCallback, useState } from 'react';
import PropTypes from 'prop-types';

// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FilledInput from '@material-ui/core/FilledInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import VisibilityRoundedIcon from '@material-ui/icons/VisibilityRounded';
import VisibilityOffRoundedIcon from '@material-ui/icons/VisibilityOffRounded';
import IconButton from '@material-ui/core/IconButton';

import { format } from '../../utils';

const MdInput = memo(
  forwardRef((props, ref) => {
    const classes = useStyles();
    const {
      id,
      variant,
      type,
      value,
      onChange,
      onBlur,
      labelText,
      helperText,
      icon,
      isFullWidth,
      isDense,
      isDisable,
      isError,
      isShowPassword,
      overrideClasses,
      textEndAdornment,
      autoComplete,
      step,
      autoFocus,
      onKeyUp,
      isShowLabelText,
      name,
      variantClasses,
      onInput,
      multiline,
      rows,
      maxLength,
    } = props;

    const [showPassword, setShowPassword] = useState(false);

    const checkType = useCallback(
      (type) => {
        return type !== 'password' ? type : showPassword ? 'text' : 'password';
      },
      [showPassword],
    );

    const endAdornment = (icon) => {
      return icon ? <InputAdornment position="end">{icon}</InputAdornment> : null;
    };

    const handleClickShowPassword = useCallback(() => {
      setShowPassword(!showPassword);
    }, [showPassword]);

    const renderPasswordToggler = useCallback(
      (type, isShowPassword, icon, textEndAdornment) => {
        if (type === 'password' && isShowPassword && format.checkIsNullOrEmpty(textEndAdornment)) {
          return (
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={() => handleClickShowPassword()}
                edge="end"
              >
                {showPassword ? <VisibilityOffRoundedIcon /> : <VisibilityRoundedIcon />}
              </IconButton>
            </InputAdornment>
          );
        } else if (!format.checkIsNullOrEmpty(textEndAdornment)) {
          return <InputAdornment position="end">{textEndAdornment}</InputAdornment>;
        } else {
          return endAdornment(icon);
        }
      },
      [handleClickShowPassword, showPassword],
    );

    const inputVariant = useCallback(
      (
        variant,
        id,
        type,
        value,
        onChange,
        onBlur,
        isShowPassword,
        icon,
        textEndAdornment,
        autoComplete,
        step,
        autoFocus,
        onKeyUp,
        name,
        variantClasses,
        onInput,
        multiline,
        rows,
        maxLength,
      ) => {
        const inputProps = {
          id: id,
          type: checkType(type),
          value: value,
          onChange: onChange,
          onBlur: onBlur,
          placeholder: labelText !== undefined ? null : id.charAt(0).toUpperCase() + id.slice(1),
          endAdornment: renderPasswordToggler(type, isShowPassword, icon, textEndAdornment),
          error: variant !== 'outlined' ? null : isError,
          autoComplete: autoComplete,
          autoFocus: autoFocus,
          onKeyUp: onKeyUp,
          name: name,
          classes: variantClasses,
          onInput: onInput,
          multiline: multiline,
          rows: rows,
          maxLength: maxLength,
        };

        switch (variant) {
          case 'outlined':
            return (
              <OutlinedInput
                style={{ backgroundColor: 'white' }}
                {...inputProps}
                inputProps={{
                  step: step,
                  maxLength: maxLength,
                }}
                label={labelText}
              />
            );
          case 'filled':
            return (
              <FilledInput
                style={{ backgroundColor: 'white' }}
                {...inputProps}
                inputProps={{
                  step: step,
                  maxLength: maxLength,
                }}
              />
            );
          default:
            return (
              <Input
                style={{ backgroundColor: 'white' }}
                {...inputProps}
                inputProps={{
                  step: step,
                  maxLength: maxLength,
                }}
              />
            );
        }
      },
      [checkType, isError, labelText, renderPasswordToggler],
    );

    return (
      <FormControl
        fullWidth={isFullWidth}
        variant={variant}
        margin={isDense ? 'dense' : 'none'}
        disabled={isDisable}
        classes={overrideClasses}
      >
        {isShowLabelText && labelText !== undefined ? (
          <InputLabel htmlFor={id}>{labelText}</InputLabel>
        ) : null}
        {inputVariant(
          variant,
          id,
          type,
          value,
          onChange,
          onBlur,
          isShowPassword,
          icon,
          textEndAdornment,
          autoComplete,
          step,
          autoFocus,
          onKeyUp,
          name,
          variantClasses,
          onInput,
          multiline,
          rows,
          maxLength,
        )}
        {helperText !== undefined ? (
          <FormHelperText id={id + '-text'} className={classes.helpText}>
            {helperText}
          </FormHelperText>
        ) : null}
      </FormControl>
    );
  }),
);

MdInput.displayName = 'MdInput';

MdInput.propTypes = {
  id: PropTypes.string,
  variant: PropTypes.oneOf(['outlined', 'filled']),
  type: PropTypes.oneOf(['text', 'password', 'time', 'number', 'search']),
  value: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  labelText: PropTypes.string,
  helperText: PropTypes.node,
  icon: PropTypes.node,
  isFullWidth: PropTypes.bool,
  isDense: PropTypes.bool,
  isDisable: PropTypes.bool,
  isError: PropTypes.bool,
  isSuccess: PropTypes.bool,
  isShowPassword: PropTypes.bool,
  overrideClasses: PropTypes.object,
  textEndAdornment: PropTypes.node,
  autoComplete: PropTypes.string,
  step: PropTypes.number,
  autoFocus: PropTypes.bool,
  onKeyUp: PropTypes.func,
  isShowLabelText: PropTypes.bool,
  name: PropTypes.string,
  variantClasses: PropTypes.object,
  onInput: PropTypes.func,
  multiline: PropTypes.bool,
  rows: PropTypes.number,
  maxLength: PropTypes.number,
};

MdInput.defaultProps = {
  variant: 'outlined',
  type: 'text',
  isFullWidth: true,
  isDisable: false,
  isShowPassword: true,
  overrideClasses: null,
  variantClasses: null,
  autoComplete: 'off',
  step: 1,
  autoFocus: false,
  isShowLabelText: true,
};

const useStyles = makeStyles((theme) => ({
  helpText: {
    color: `${theme.palette.error.main}`,
  },
}));

export default MdInput;
