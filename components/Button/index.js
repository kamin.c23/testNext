import React, { useMemo, useCallback, useState } from 'react';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';

const MdButton = (props) => {
  const { id, variant, type, text, outlined, ...rest } = props;
  const classes = useStyles();

  const ButtonComponent = useMemo(
    () => (
      <Button
        id={id}
        variant={variant}
        color="primary"
        type={type}
        classes={{
          root: outlined ? classes.outlinedButton : classes.button,
        }}
        {...rest}
      >
        {text}
      </Button>
    ),
    [id, variant, type, text, rest],
  );

  return <>{ButtonComponent}</>;
};

MdButton.propTypes = {
  id: PropTypes.string,
  variant: PropTypes.string,
  type: PropTypes.string,
  text: PropTypes.string,
  outlined: PropTypes.bool,
};

MdButton.defaultProps = {};

const useStyles = makeStyles((theme) => ({
  outlinedButton: {
    textTransform: 'none',
    marginLeft: '3px',
    marginRight: '3px',
  },
  button: {
    // width: '190px',
    // height: '48px',
    // borderRadius: '50px',
    color: '#fff',
    background: theme.palette.primary[600],
    '@media (max-width: 860px)': {
      width: '95px',
    },
    '&:hover': {
      background: theme.palette.primary[700],
    },
  },
}));

export default React.memo(MdButton);
