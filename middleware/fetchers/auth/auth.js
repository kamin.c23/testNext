import { requestHandler } from '../../apiHandler';

// eslint-disable-next-line no-undef
const host = process.env.NEXT_PUBLIC_API_URL || '';

export const requestTest = () =>
  requestHandler({
    useMock: false,
    method: 'get',
    url: `https://cpfc-dev.truecorp.co.th/api/course/browse?page=1&pageSize=5`,
  });
