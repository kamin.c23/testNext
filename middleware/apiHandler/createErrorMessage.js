const errorMessage = (res) => {
  if (res.status === 200 && !res.data.success) return 'Error message 200';
  if (res.status === 400) return 'Error message 400';
  if (res.status === 401) return 'Error message 401';
  if (res.status === 403) return 'Error message 403';
  if (res.status === 404) return 'Error message 404';
  if (res.status === 500) return `Internal Server Error`;

  return res.statusText;
};
export const getErrorMessage = (res) => {
  if (res.response) {
    const { data } = res.response;
    return {
      error: {
        type: 'ERROR',
        status: res.status,
        statusText: errorMessage(res),
        message: data.message,
        success: data.success,
      },
    };
  } else {
    return {
      error: {
        type: 'ERROR',
        code: res.code,
        res: res,
      },
    };
  }
};
