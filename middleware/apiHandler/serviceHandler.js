export const isServiceError = (res) => {
  if (res === undefined) return true;
  if (res.status !== 200 && res.status !== 499) return true;
  if (res.status === 200 && res.data.success === false) return true;
  if (res.data.error) return true;
  if (res.data.fault) return true;
  if (res.data.IsError) return true;
  return false;
};
