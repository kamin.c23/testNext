import axios from 'axios';

import { isServiceError } from './serviceHandler';
import { getErrorMessage } from './createErrorMessage';

const defaultOptions = {
  url: '',
  params: {},
  data: {},
};

export const requestHandler = (userOptions) => {
  const options = { ...defaultOptions, ...userOptions };
  return new Promise((resolve) => {
    resolve(fetcher(options));
  });
};

const fetcher = async (options) => {
  const { url } = options;
  let res;

  try {
    res = await axios(options);
    console.log(`${url} | Response =>`, res);

    if (isServiceError(res, options)) {
      return getErrorMessage(res.error, options);
    }
  } catch (error) {
    console.log(`${url} | Error =>`, error);
    if (res === undefined && isServiceError(error.response, options)) {
      return getErrorMessage(error, options);
    }
  }

  return res.data;
};
