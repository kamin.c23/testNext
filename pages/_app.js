// import '../App.css';
import { hot } from 'react-hot-loader';
// import Layout from '../components/Layouts/Layout';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core';
import * as utils from '../utils';
import theme from '../theme';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import Base from '../components/style/Base';
import Layout1 from '../components/Layouts/Layout1';
import Layout2 from '../components/Layouts/Authenticated';
import { useEffect } from 'react';
import '../assets/admin-lte.css';
import '../assets/antd-table.css';
import 'typeface-roboto';
import './index.css';
const layouts = {
  L1: Layout1,
  L2: Layout2,
};
const MyApp = ({ Component, pageProps }) => {
  console.log(process.env);
  // const history = utils.history;
  useEffect(() => {
    // on initial load -> remove server-side injected CSS.
    console.log('========================= delete crash =========================');
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  const Layout = layouts[Component.layout] || ((children) => <>{children}</>);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Base />
      <ToastContainer />
      <div className="App">
        <Provider store={utils.store.storeConfigure()}>
          {/* <Router history={history}>
            <HistoryListener> */}
          {/* <Layout /> */}

          <Layout>
            <Component {...pageProps} />
          </Layout>
          {/* </HistoryListener>
          </Router> */}
        </Provider>
      </div>
    </ThemeProvider>
  );
};

export default process.env.NODE_ENV === 'development' ? hot(module)(MyApp) : MyApp;
