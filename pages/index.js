import React from 'react';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Input from '../components/Input';
import { GridContainer, GridItem } from '../components/Grid';
import LoginStyles from '../components/style/Login';
import * as actions from '../actions';
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';
import Grid from '@material-ui/core/Grid';
import { useRouter } from 'next/router';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(20),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  submitFail: { margin: theme.spacing(1, 0, 2) },
  helpText: {
    color: `${theme.palette.error.main}`,
    padding: '0px 14px 0px',
    fontWeight: '400',
    backgroundColor: '#FFFFFF',
  },
  alertField: {
    border: '.1em solid #eac8d0',
    margin: '0px 10px 10px',
    backgroundColor: 'rgb(249, 210, 215) !important',
    color: '#693041 !important',
    '& .MuiAlert-icon': {
      color: '#F44336 !important',
    },
  },

  txtLogin: {
    textAlign: 'left',
    marginLeft: '3rem',
    marginTop: '40vh',
  },
  onMobileBg: {
    '@media (max-width: 959px)': {
      display: 'none',
    },
  },
}));

const SignIn = (props) => {
  const classes = useStyles();

  const [isShowError, setShowError] = React.useState(true);
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [err, setErr] = React.useState(false);
  const [errLogin, setErrorLogin] = React.useState('');
  const dispatch = useDispatch();
  const router = useRouter();
  const onTypeUsername = (event) => {
    setUsername(event.target.value);
  };

  const onTypePassword = (event) => {
    setPassword(event.target.value);
  };

  const onSubmit = async () => {
    alert('test');
    setErr(true);

    const credentials = {
      username: username,
      password: password,
    };

    setShowError(true);
    if (!username && !password) {
      return;
    }
    dispatch(actions.authen.signIn(credentials, router));
    // setErrorLogin(error.message);
    // .then((error) => {
    //   if (error) setErrorLogin(error.message);
    // });
  };

  return (
    //    <Grid item sm={7} md={7} lg={7}>
    //   <Img alt="complex" src={bg} style={{ height: '100vh' }} />
    // </Grid>
    <Grid container component="main">
      {' '}
      <Container component="main" maxWidth="xs">
        <LoginStyles />
        <div className={classes.paper}>
          <GridContainer style={{ padding: '5px 0', textAlign: 'center' }}>
            <GridItem xs={12} sm={12} md={12} lg={12}>
              <img
                src={'/tnn-logo.png'}
                alt="logo-true-world"
                height="100"
                width="100"
                // className={classes.connectImg}
              />{' '}
              {/* <Image
                src="https://www.borntodev.com/wp-content/uploads/2021/01/medal-1.png"
                alt="train to dev"
                width={1916}
                height={1020}
              /> */}
            </GridItem>
          </GridContainer>
          <Typography component="h1" variant="h4" style={{ padding: '5px 0', color: '#FFFFFF' }}>
            Sign in to your account
          </Typography>
          <form className={classes.form} noValidate>
            <GridContainer style={{ padding: '5px 0' }}>
              {err && errLogin != '' ? (
                // <GridItem xs={12} sm={12} md={12} lg={12} style={{ textAlign: 'left' }}>
                //   {' '}
                //   <label id={'login-error-text'} className={classes.helpText}>
                //     {errLogin}
                //   </label>
                // </GridItem>
                <Stack sx={{ width: '100%' }} spacing={2}>
                  <Alert
                    // icon={true}
                    // variant="outlined"
                    severity="error"
                    // style={{ backgroundColor: '#f9d2d7', margin: '0 10px 0 10px' }}
                    className={classes.alertField}
                  >
                    <label style={{ margin: '0px' }}> {errLogin}</label>
                  </Alert>
                </Stack>
              ) : null}
              <GridItem xs={12} sm={12} md={12} lg={12}>
                <Input
                  id="username"
                  labelText={'Username'}
                  variant="outlined"
                  isDense={true}
                  isFullWidth={true}
                  value={username}
                  onChange={onTypeUsername}
                  style={{ color: 'white' }}
                  // isError={!!error}
                  // helperText={error ? error.message : null}
                  helperText={err && username.length <= 0 ? 'Please Enter Username' : null}
                  autoComplete="off"
                />{' '}
              </GridItem>
            </GridContainer>

            <GridContainer style={{ padding: '5px 0' }}>
              <GridItem xs={12} sm={12} md={12} lg={12}>
                <Input
                  id="password"
                  labelText={'Password'}
                  variant="outlined"
                  isDense={true}
                  isFullWidth={true}
                  value={password}
                  onChange={onTypePassword}
                  // isError={!!error}
                  // helperText={error ? error.message : null}
                  helperText={err && password.length <= 0 ? 'Please Enter Password' : null}
                  type="password"
                />
              </GridItem>
            </GridContainer>
            <GridContainer>
              <GridItem xs={12} sm={12} md={12} lg={12}>
                {/* {err && errLogin != '' ? (
              // <GridItem xs={12} sm={12} md={12} lg={12} style={{ textAlign: 'left' }}>
              //   {' '}
              //   <label id={'login-error-text'} className={classes.helpText}>
              //     {errLogin}
              //   </label>
              // </GridItem>
              <FormHelperText id={'login-error-text'} className={classes.helpText}>
                {errLogin}
              </FormHelperText>
            ) : null} */}

                <Button
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={err && errLogin != '' ? classes.submitFail : classes.submit}
                  onClick={() => onSubmit()}
                >
                  Sign In
                </Button>
              </GridItem>
            </GridContainer>
          </form>
        </div>
      </Container>
    </Grid>
  );
};

const mapStateToProps = (state) => ({
  error: state.auth.error,
});
SignIn.layout = 'L1';

export default SignIn;
