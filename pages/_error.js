import { makeStyles } from '@material-ui/core/styles';

import Meta from '../components/MetaTag';
import Hero from '../components/Hero';

const Error = () => {
  const classes = useStyles();
  return (
    <>
      <Meta title="Server Error" />
      <Hero type="primary">
        <div className={classes.container}>
          <h1>500</h1>
          <h2>Server Error</h2>
        </div>
      </Hero>
    </>
  );
};

const useStyles = makeStyles((theme) => ({
  container: {
    // height: `calc(100vh - ${theme.footer.height})`,
    height: `100vh`,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFF',

    '& > h1': {
      fontSize: '13rem',
      height: '230px',
    },

    '& > h2': {
      fontSize: '3rem',
      fontWeight: '400',
    },
  },
}));
Error.layout = 'L1';

export default Error;
