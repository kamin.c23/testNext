import React, { useState, useEffect } from 'react';
import { GridContainer, GridItem } from '../../components/Grid';
import BlockUi from 'react-block-ui';
import Input from '../../components/Input';
import SelectMenu from '../../components/SelectMenu';
import Collapse from '@material-ui/core/Collapse';
import Button from '@material-ui/core/Button';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import { makeStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import SearchIcon from '@material-ui/icons/Search';
import DataTable from '../../components/DataTable/DataTable';
import { Label } from 'reactstrap';
import UserStyles from '../../components/style/User';
import AddIcon from '@material-ui/icons/Add';
import Link from 'next/link';
import EditIcon from '@material-ui/icons/Edit';

const User = (props) => {
  useEffect(() => {
    (async function anyNameFunction() {})();
  }, []);
  const [open, setOpen] = React.useState(false);
  const [modal2, setModal2] = useState(false);
  const [viewData, setViewData] = useState({});
  const toggle2 = () => {
    setModal2(!modal2);
    if (modal2) {
      // setDatas([]);
    }
  };
  const handleClick = () => {
    setOpen(!open);
  };
  const statusBadge = (data) => {
    switch (data) {
      case 'Y':
        return (
          <Label
            style={{
              fontSize: '90%',
              padding: '5px',
              color: 'green',
              fontWeight: 'bold',
            }}
          >
            Active
          </Label>
        );
      case 'N':
        return (
          <Label
            style={{
              fontSize: '90%',
              padding: '5px',
              color: 'red',
              fontWeight: 'bold',
            }}
          >
            In-Active
          </Label>
        );
      default:
        return (
          <Label style={{ fontSize: '90%', padding: '5px', color: 'red', fontWeight: 'bold' }}>
            {data}
          </Label>
        );
    }
  };
  const columns = [
    {
      title: 'ID',
      dataIndex: 'user_id',
      key: 'user_id',
      showSorterTooltip: false,
      onClick: (e) => {
        console.log('Content: ', e.currentTarget.dataset.id);
      },
      onCell: (record, rowIndex) => ({
        onClick: () => {
          setViewData(record);
          toggle2();
        },
      }),
    },
    {
      title: 'PIN CODE',
      dataIndex: 'pin_code',
      key: 'pin_code',
      showSorterTooltip: false,

      // sorter: (a, b) => a.news_type.length - b.news_type.length,
      onCell: (record, rowIndex) => ({
        onClick: () => {
          setViewData(record);

          toggle2();
        },
      }),
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      showSorterTooltip: false,
      onCell: (record, rowIndex) => ({
        onClick: () => {
          setViewData(record);

          toggle2();
        },
      }),
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      showSorterTooltip: false,
      render: (text, record) => <div>{statusBadge(record.status)}</div>,
      onCell: (record, rowIndex) => ({
        onClick: () => {
          setViewData(record);

          toggle2();
        },
      }),
    },
    {
      title: 'Admin',
      dataIndex: 'admin_flag',
      key: 'admin_flag',
      showSorterTooltip: false,
      render: (text, record) => <div>{statusBadge(record.admin_flag)}</div>,
      onCell: (record, rowIndex) => ({
        onClick: () => {
          setViewData(record);

          toggle2();
        },
      }),
    },
    // {
    //   title: 'วันที่ออนแอร์',
    //   dataIndex: 'onair_date_start',
    //   key: 'onair_date_start',
    //   showSorterTooltip: false,
    //   render: (text, record) => {
    //     return (
    //       <div>
    //         {moment(record.onair_date_start).format('DD/MM/YYYY')}{' '}
    //         {moment(record.onair_date_start).format('HH:MM')} - <br />
    //         {moment(record.onair_date_end).format('DD/MM/YYYY')}{' '}
    //         {moment(record.onair_date_end).format('HH:MM')}
    //       </div>
    //     );
    //   },
    //   onCell: (record, rowIndex) => ({
    //     onClick: () => {
    //       setViewData(record);

    //       toggle2();
    //     },
    //   }),
    // },
    // {
    //   title: 'Actions',
    //   showSorterTooltip: false,
    //   render: (actions) =>
    //     actions &&
    //     actions.map((action) => (
    //       // eslint-disable-next-line react/jsx-key
    //       <InteractiveModal
    //         name={'แก้ไขข้อความข่าว'}
    //         idNews={'XX-AASSSFFF'}
    //         status={'edit'}
    //         className="action"
    //       />
    //     )),
    //   className: 'actions',
    // },
    {
      title: '',
      dataIndex: 'actions',
      render: (text, record) => (
        <Link href={`/user/${record.user_id}`}>
          <a>
            <Button
              variant="contained"
              style={{ backgroundColor: 'unset', 'box-shadow': 'none', color: '#F65C35' }}
              startIcon={<EditIcon />}
              disabled={props.disabled}
              className="actions"
            >
              แก้ไข
            </Button>
          </a>
        </Link>
      ),
      // className: 'actions',
    },
  ];
  const aaa = [
    {
      user_id: 3,
      pin_code: '543903',
      name: 'Kamin Chuangsuwanich',
      status: 'Y',
      admin_flag: 'N',
    },
  ];
  const [datas, setDatas] = useState(aaa);
  const [newsTypeList, setNewsTypeList] = useState([
    { id: 'สถานการณ์ประจำวัน', value: 'สถานการณ์ประจำวัน' },
    { id: 'ข่าวเศรษฐกิจ', value: 'ข่าวเศรษฐกิจ' },
    { id: 'ข่าวต่างประเทศ', value: 'ข่าวต่างประเทศ' },
    { id: 'ข่าวกีฬา', value: 'ข่าวกีฬา' },
  ]);
  const [newsStatusList, setNewsStatusList] = useState([
    { id: 'Y', value: 'Active' },
    { id: 'N', value: 'In-Active' },
  ]);
  const useStyles = makeStyles((theme) => ({
    root: {
      justifyContent: 'left',
    },
    margin: {
      margin: theme.spacing(1),
    },
    formControl: {
      minWidth: 170,
    },
    input: {
      minWidth: '100%',
    },
    mgRight: {
      marginRight: '1rem',
    },
    inputCol: {
      textAlign: 'left',
    },
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },

    btn: { marginTop: '0.5rem' },
    mgTop: { marginTop: '0.5rem' },
    timePicker: { maxWidth: '12rem' },
  }));
  const classes = useStyles();
  return (
    <div style={{ marginLeft: '3rem', marginRight: '3rem' }}>
      <BlockUi>
        <UserStyles />
        <GridContainer style={{ padding: '5px 0' }}>
          <GridItem xs={12} sm={12} md={3} lg={2}>
            <Input
              id="pin_code"
              labelText={'PIN CODE'}
              variant="outlined"
              isDense={true}
              isFullWidth={true}
              className={'test'}
              // value={username}
              // onChange={onTypeUsername}
              // style={{ color: 'white' }}
              // isError={!!error}
              // helperText={error ? error.message : null}
              autoComplete="off"
            />{' '}
          </GridItem>{' '}
          <GridItem xs={12} sm={12} md={3} lg={3} style={{ textAlign: 'left' }}>
            <Input
              id="name"
              labelText={'Name'}
              variant="outlined"
              isDense={true}
              isFullWidth={true}
              className={'test'}
              // value={username}
              // onChange={onTypeUsername}
              // style={{ color: 'white' }}
              // isError={!!error}
              // helperText={error ? error.message : null}
              autoComplete="off"
            />{' '}
          </GridItem>
          <GridItem xs={12} sm={12} md={2} lg={2} style={{ textAlign: 'left' }}>
            <Button
              fullWidth
              variant="contained"
              color="#FFFFFF"
              className={classes.btn}
              style={{ width: 'auto', backgroundColor: 'white' }}
              endIcon={open ? <ExpandLessIcon /> : <ExpandMoreIcon />}
              onClick={() => handleClick()}

              // onClick={() => onSubmit()}
            >
              More
            </Button>
          </GridItem>
          <GridItem xs={12} sm={12} md={4} lg={5} style={{ textAlign: 'right' }}>
            <Link href={'/user/new'}>
              <a>
                <Button
                  variant="contained"
                  color="primary"
                  startIcon={<AddIcon />}
                  disabled={props.disabled}
                >
                  Add User{' '}
                </Button>{' '}
              </a>
            </Link>
          </GridItem>
        </GridContainer>

        <Collapse in={open} timeout="auto" unmountOnExit>
          <GridContainer>
            <GridItem
              xs={12}
              sm={12}
              md={2}
              lg={2}
              style={{ textAlign: 'left' }}
              className={classes.mgTop}
            >
              <Input
                id="user_id"
                labelText={'ID'}
                variant="outlined"
                isDense={true}
                isFullWidth={true}
                className={'test'}
                // value={username}
                // onChange={onTypeUsername}
                // style={{ color: 'white' }}
                // isError={!!error}
                // helperText={error ? error.message : null}
                autoComplete="off"
              />{' '}
            </GridItem>
            <GridItem
              xs={12}
              sm={12}
              md={2}
              lg={2}
              style={{ textAlign: 'left' }}
              className={classes.mgTop + ' ' + classes.timePicker}
            >
              <SelectMenu
                list={newsStatusList}
                // value={sort}
                label={'Status'}
                // onChange={onSort}
              />
            </GridItem>{' '}
            <GridItem
              xs={12}
              sm={12}
              md={2}
              lg={2}
              style={{ textAlign: 'left' }}
              className={classes.mgTop}
            >
              <SelectMenu
                list={newsStatusList}
                // value={sort}
                label={'Admin'}
                // onChange={onSort}
              />
            </GridItem>
          </GridContainer>
        </Collapse>

        <GridContainer>
          {' '}
          <GridItem>
            <Button
              fullWidth
              variant="contained"
              color="#FFFFFF"
              className={classes.btn}
              style={{
                width: 'auto',
                backgroundColor: '#FFF1F2',
                color: '#F65C35',
                border: '1px solid #F65C35',
              }}
              startIcon={<SearchIcon />}

              // onClick={() => onSubmit()}
            >
              ค้นหา
            </Button>
          </GridItem>
          <GridItem>
            <Button
              fullWidth
              variant="contained"
              color="#FFFFFF"
              className={classes.btn}
              style={{
                width: 'auto',
                backgroundColor: 'unset',
                'box-shadow': 'none',
                color: '#F65C35',
              }}
            >
              ล้าง
            </Button>
          </GridItem>
        </GridContainer>

        <GridContainer style={{ marginTop: '2rem' }}>
          {' '}
          <GridItem xs={12} sm={12} md={12} lg={12} style={{ paddingRight: '0px' }}>
            <DataTable
              // onclick={onclick}
              columns={columns}
              pageSize={10}
              //  expandable={{expandedRowRender}}
              datas={datas}
              // loading={loadTable}
              // scroll={{ x: '100%' }}
              // onRowsClick={toggle2}
            />
          </GridItem>
          {/* {modal2 ? (
            <InteractiveViewModal
              name={'ข้อความข่าว'}
              idNews={'XX-AASSSFFF'}
              status={'edit'}
              modal2={modal2}
              toggle={toggle2}
            />
          ) : (
            <div />
          )} */}{' '}
        </GridContainer>
      </BlockUi>
    </div>
  );
};
User.layout = 'L2';

export default User;
