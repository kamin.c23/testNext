import React, { useState, useEffect } from 'react';
import { GridContainer, GridItem } from '../../components/Grid';
import BlockUi from 'react-block-ui';
import Input from '../../components/Input';
import { makeStyles } from '@material-ui/core/styles';
import UserDetailStyles from '../../components/style/User/detail';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import TabPanel from '../../components/TabPanel';
import Box from '@mui/material/Box';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import { Spinner } from 'reactstrap';

import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';

import Button from '../../components/Button';

// import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import CardHeader from '@mui/material/CardHeader';
const UserDetail = (props) => {
  useEffect(() => {
    (async function anyNameFunction() {})();
  }, []);

  const [newsStatusList, setNewsStatusList] = useState([
    { id: 'Y', value: 'Active' },
    { id: 'N', value: 'In-Active' },
  ]);
  const useStyles = makeStyles((theme) => ({
    root: {
      justifyContent: 'left',
    },
    margin: {
      margin: theme.spacing(1),
    },
    formControl: {
      minWidth: 170,
    },
    input: {
      minWidth: '100%',
    },
    mgRight: {
      marginRight: '1rem',
    },
    inputCol: {
      textAlign: 'left',
    },
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },

    btn: { marginTop: '0.5rem' },
    mgTop: { marginTop: '0.5rem' },
    timePicker: { maxWidth: '12rem' },
  }));
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [isLoading, setIsLoading] = useState(false);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  function a11yProps(index) {
    return {
      id: `vertical-tab-${index}`,
      'aria-controls': `vertical-tabpanel-${index}`,
    };
  }
  const bull = (
    <Box component="span" sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}>
      •
    </Box>
  );
  return (
    <div style={{ marginLeft: '3rem', marginRight: '3rem' }}>
      <BlockUi>
        <UserDetailStyles />
        <Card sx={{ minWidth: 275 }}>
          <CardHeader
            className={classes.header}
            component={Typography}
            title={'User Detail'}
            // subheader={'Subtitle Text'}
          />
          <CardContent>
            <GridContainer>
              <GridItem xs={12} sm={12} md={2} lg={2}>
                <FormControl className={classes.margin + ' ' + classes.input}>
                  <Input
                    id="user_id"
                    labelText={'ID'}
                    isDense={true}
                    isFullWidth={true}
                    className={'test'}
                    value={'3'}
                    disabled
                    // onChange={onTypeUsername}
                    // style={{ color: 'white' }}
                    // isError={!!error}
                    // helperText={error ? error.message : null}
                    autoComplete="off"
                  />{' '}
                </FormControl>
              </GridItem>
              <GridItem xs={12} sm={12} md={3} lg={3}>
                <FormControl className={classes.margin + ' ' + classes.input}>
                  <Input
                    id="pin_code"
                    labelText={'PIN CODE'}
                    isDense={true}
                    isFullWidth={true}
                    className={'test'}
                    value={'543903'}
                    // onChange={onTypeUsername}
                    // style={{ color: 'white' }}
                    // isError={!!error}
                    // helperText={error ? error.message : null}
                    autoComplete="off"
                  />{' '}
                </FormControl>
              </GridItem>
            </GridContainer>{' '}
            <GridContainer>
              <GridItem xs={12} sm={12} md={3} lg={3}>
                <FormControl className={classes.margin + ' ' + classes.input}>
                  <Input
                    id="login_name"
                    labelText={'Login Name'}
                    isDense={true}
                    isFullWidth={true}
                    className={'test'}
                    value={'Kamin'}
                    disabled
                    // onChange={onTypeUsername}
                    // style={{ color: 'white' }}
                    // isError={!!error}
                    // helperText={error ? error.message : null}
                    autoComplete="off"
                  />{' '}
                </FormControl>
              </GridItem>
              <GridItem xs={12} sm={12} md={3} lg={3}>
                <FormControl className={classes.margin + ' ' + classes.input}>
                  <Input
                    id="first_name"
                    labelText={'First Name'}
                    isDense={true}
                    isFullWidth={true}
                    className={'test'}
                    value={'Kamin'}
                    // onChange={onTypeUsername}
                    // style={{ color: 'white' }}
                    // isError={!!error}
                    // helperText={error ? error.message : null}
                    autoComplete="off"
                  />{' '}
                </FormControl>
              </GridItem>
              <GridItem xs={12} sm={12} md={3} lg={3}>
                <FormControl className={classes.margin + ' ' + classes.input}>
                  <Input
                    id="last_name"
                    labelText={'Last Name'}
                    isDense={true}
                    isFullWidth={true}
                    className={'test'}
                    value={'Chuangsuwanich'}
                    // onChange={onTypeUsername}
                    // style={{ color: 'white' }}
                    // isError={!!error}
                    // helperText={error ? error.message : null}
                    autoComplete="off"
                  />{' '}
                </FormControl>
              </GridItem>
            </GridContainer>{' '}
            <GridContainer>
              <GridItem
                xs={12}
                sm={12}
                md={6}
                lg={6}
                style={{ textAlign: 'left' }}
                className={classes.mgTop}
              >
                {' '}
                <FormControl className={classes.margin + ' ' + classes.input}>
                  <Input
                    id="email"
                    labelText={'Email'}
                    isDense={true}
                    isFullWidth={true}
                    value={'kamin.c23@gmail.com'}
                    // value={username}
                    // onChange={onTypeUsername}
                    // style={{ color: 'white' }}
                    // isError={!!error}
                    // helperText={error ? error.message : null}
                    autoComplete="off"
                  />{' '}
                </FormControl>
              </GridItem>
              <GridItem
                xs={6}
                sm={6}
                md={2}
                lg={2}
                style={{ textAlign: 'left' }}
                className={classes.mgTop + ' ' + classes.timePicker}
              >
                {' '}
                <FormControl className={classes.margin + ' ' + classes.input}>
                  <FormGroup style={{ marginTop: '20px', marginLeft: '1rem' }}>
                    <FormControlLabel control={<Checkbox defaultChecked />} label="Valid" />
                  </FormGroup>
                </FormControl>
              </GridItem>{' '}
              <GridItem
                xs={6}
                sm={6}
                md={2}
                lg={2}
                style={{ textAlign: 'left' }}
                className={classes.mgTop + ' ' + classes.timePicker}
              >
                {' '}
                <FormControl className={classes.margin + ' ' + classes.input}>
                  <FormGroup style={{ marginTop: '20px' }}>
                    <FormControlLabel control={<Checkbox defaultChecked />} label="Admin" />
                  </FormGroup>
                </FormControl>
              </GridItem>{' '}
            </GridContainer>
            <GridContainer>
              {' '}
              <GridItem
                xs={12}
                sm={12}
                md={12}
                lg={12}
                style={{ textAlign: 'left' }}
                className={classes.mgTop}
              >
                <Box
                  sx={{ flexGrow: 1, bgcolor: 'background.paper', display: 'flex', height: 300 }}
                  style={{
                    borderRadius: '10px !important',
                    boxShadow: '0px 0px 3px 2px rgb(0 0 0 / 9%)',
                  }}
                >
                  <Tabs
                    orientation="vertical"
                    variant="scrollable"
                    value={value}
                    onChange={handleChange}
                    aria-label="Vertical tabs example"
                    sx={{ borderRight: 1, borderColor: 'divider' }}
                    style={{ width: '150px' }}
                  >
                    <Tab label="Item One" {...a11yProps(0)} />
                    <Tab label="Item Two" {...a11yProps(1)} />
                    <Tab label="Item Three" {...a11yProps(2)} />
                    <Tab label="Item Four" {...a11yProps(3)} />
                    <Tab label="Item Five" {...a11yProps(4)} />
                    <Tab label="Item Six" {...a11yProps(5)} />
                    <Tab label="Item Seven" {...a11yProps(6)} />
                  </Tabs>
                  <TabPanel value={value} index={0}>
                    Item One
                  </TabPanel>
                  <TabPanel value={value} index={1}>
                    Item Two
                  </TabPanel>
                  <TabPanel value={value} index={2}>
                    Item Three
                  </TabPanel>
                  <TabPanel value={value} index={3}>
                    Item Four
                  </TabPanel>
                  <TabPanel value={value} index={4}>
                    Item Five
                  </TabPanel>
                  <TabPanel value={value} index={5}>
                    Item Six
                  </TabPanel>
                  <TabPanel value={value} index={6}>
                    Item Seven
                  </TabPanel>
                </Box>
              </GridItem>
            </GridContainer>
          </CardContent>
          <CardActions>
            <Button
              // onClick={toggle}
              variant="contained"
              style={{
                marginRight: '5px',
                backgroundColor: 'unset',
                'box-shadow': 'none',
                color: '#8E8E8E',
              }}
              text={'Cancel'}
            ></Button>
            {isLoading ? (
              <Button color="primary" disabled text={'Save'}>
                <Spinner animation="border" size="sm" role="status" />
                <span style={{ paddingLeft: 10 }}>Save</span>
              </Button>
            ) : (
              <Button
                color="primary"
                variant="contained"
                //  onClick={toggle}
                text={'Save'}
              ></Button>
            )}{' '}
          </CardActions>
        </Card>{' '}
      </BlockUi>
    </div>
  );
};
UserDetail.layout = 'L2';

export default UserDetail;
