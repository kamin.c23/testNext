import axios from 'axios';
import * as constants from '../../constants';
import { menu, user } from '../../constants';
import * as utils from '../../utils';
import { useRouter } from 'next/router';

let cancel = null;

const menuItems = [
  {
    href: '/user',
    title: 'User',
  },
  // {
  //   href: '/user',
  //   title: 'User 2',
  // },
  // {
  //   href: '/runningtext2',
  //   title: 'Interactive',
  // },
  // {
  //   icon: "Group",
  //   title: "User Management",
  //   href: "/users",
  //   childs: [
  //     {
  //       href: "/users/employee",
  //       icon: "GroupAdd",
  //       title: "Users",
  //     },
  //     {
  //       href: "/users/privilege",
  //       icon: "LockOpen",
  //       title: "User Privilege",
  //       divider: true
  //     },
  //     {
  //       href: "/users/master/business-unit",
  //       icon: "BusinessCenter",
  //       title: "Business Unit",
  //     },
  //     {
  //       href: "/users/master/role",
  //       icon: "Assignment",
  //       title: "Role",
  //     },
  //     {
  //       href: "/users/master/category",
  //       icon: "Category",
  //       title: "Category",
  //     },
  //     {
  //       href: "/users/master/privilege",
  //       icon: "Lock",
  //       title: "Privilege",
  //     },
  //   ]
  // },
  // {
  //   icon: "DesktopWindows",
  //   title: "Monitor",
  //   href: "/monitor",
  //   childs: [
  //     {
  //       href: "/monitor/minio",
  //       iconImg: "/minio_icon.png",
  //       title: "Minio",
  //     }
  //   ]
  // }
];

const signIn = (credentials, router) => {
  // const dispatch = useDispatch();

  return (dispatch) => {
    let cancelCallAPI = new axios.CancelToken(function executor(c) {
      cancel = c;
    });
    dispatch({
      type: user.userInfoConstants.SAVE_USERINFO,
      payload: { name: 'ทดสอบ เข้าใช้งาน' },
    });
    dispatch({ type: menu.menuConstants.SET_MENU, payload: menuItems });
    router.push(menuItems[0].href);
  };
};

const refreshToken = (tokenObj) => {
  return (dispatch) => {
    dispatch({
      type: constants.authen.authenConstants.REFRESH,
      payload: {
        accessToken: tokenObj.token,
        refreshToken: tokenObj.refresh_token,
        error: tokenObj.error,
      },
    });
  };
};

const signOut = () => {
  return (dispatch) => {
    let cancelCallAPI = new axios.CancelToken((c) => {
      cancel = c;
    });
    let refreshToken = JSON.stringify(utils.auth.loadRefreshAuth());
  };
};

const verifyToken = () => {
  let cancelCallAPI = new axios.CancelToken(function executor(c) {
    // An executor function receives a cancel function as a parameter
    cancel = c;
  });
};

export { signIn, signOut, refreshToken, verifyToken };
