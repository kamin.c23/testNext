import { user } from '../../constants';

const setUserInfo = (userInfo) => {
  return (dispatch) => {
    dispatch({
      type: user.userInfoConstants.SAVE_USERINFO,
      payload: userInfo,
    });
  };
};

const clearUserInfo = () => {
  return (dispatch) => {
    dispatch({
      type: user.userInfoConstants.CLEAR_USERINFO,
    });
  };
};

export { setUserInfo, clearUserInfo };
