const { i18n } = require('./next-i18next.config');
const withImages = require('next-images');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = withImages({
  i18n,
  // webpack5: false,
  publicRuntimeConfig: {
    // Will be available on both server and client
    backendUrl: process.env.BACKEND_URL,
  },
  env: {
    url: 'http://localhost:3000',
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
  async headers() {
    return [
      {
        source: '/(.*)?',
        headers: [
          {
            key: 'X-Frame-Options',
            value: 'SAMEORIGIN',
          },
          {
            key: 'Content-Security-Policy',
            value: `frame-ancestors 'self' https://vclass.ai/ https://vroom.truevirtualworld.com/ https://trueconnect.ekoapp.com/ https://cpgconnect.ekoapp.com/ https://ekodemo.ekoapp.com/`,
          },
        ],
      },
    ];
  },
  async rewrites() {
    return [{ source: '/status', destination: '/api/status' }];
  },
  webpack: (config, options) => {
    config.resolve.alias = {
      ...config.resolve.alias,
      'react-dom$': 'react-dom/profiling',
      'scheduler/tracing': 'scheduler/tracing-profiling',
    };

    if (options.isServer) {
      require('./scripts/generate-sitemap');
    }

    // Only disable function name mangling on the env.development
    if (process.env.NODE_ENV === 'development') {
      const terser = config.optimization.minimizer.find((plugin) => plugin instanceof TerserPlugin);
      if (terser) {
        terser.options.terserOptions = {
          ...terser.options.terserOptions,
          keep_classnames: true,
          keep_fnames: true,
        };
      }
    }

    return config;
  },
  images: {
    domains: [
      'images.unsplash.com',
      'cpfc-dev.truecorp.co.th',
      'cpfc-uat.truecorp.co.th',
      'cpuniversityonline.com',
    ],
  },
});
