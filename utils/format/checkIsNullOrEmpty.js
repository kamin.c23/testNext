
export const checkIsNullOrEmpty = (val) => {
  switch (typeof val) {
    case "string":
      return (val === '' || val === 'null' || val === 'undefined' || val.length <= 0)
    case "object":
      return ((val === null) || (Object.keys(val).length <= 0))
    case "undefined":
      return true
    default:
      return false
  }
}

