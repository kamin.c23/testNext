
export const dateTimeFormat = (date, format = '') => {
  if (format) {
    return date.format(format)
  } else {
    return date.format()
  }
}

