import * as Icons from '@material-ui/icons/'

export const getMaterialIcon = (iconName) => Icons[iconName]
