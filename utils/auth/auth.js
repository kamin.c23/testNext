

export const storeAuth = (accessToken) => {
  localStorage.setItem('access-token')
}

export const storeRefreshAuth = (refreshToken) => {
  localStorage.setItem('refresh-token')
}

export const loadAuth = () => {
  let token = localStorage.getItem('access-token')
  return token 
}

export const loadRefreshAuth = () => {
  let refresh = localStorage.getItem('refresh-token')
  return refresh 
}

export const removeAuth = () => {
  localStorage.removeItem('access-token')
  localStorage.removeItem('refresh-token')
  localStorage.removeItem('store')
}

