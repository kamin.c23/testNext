

export const loadState = () => {
  try {
    let serializedState = null
    if (process.env.NODE_ENV === 'development') {
      serializedState = localStorage.getItem('store')
    } else {
      serializedState = localStorage.getItem('store')
    }
    if (serializedState === null) {
      return undefined
    } else {
      return JSON.parse(serializedState)
    }
  } catch (error) {
    return undefined
  }
}

export const saveState = (state) => {
  try {
    let serializedState = null
    if (process.env.NODE_ENV === 'development') {
      serializedState = JSON.stringify(state)
    } else {
      serializedState = JSON.stringify(state)
    }
    localStorage.setItem('store', serializedState)

  } catch (error) {
    return undefined
  }
}

 