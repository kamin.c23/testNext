import { createStore, applyMiddleware } from 'redux'
import { loadState, saveState } from './localStorage'
import ReduxThunk from 'redux-thunk'
import rootReducer from '../../reducers'

const persistStore = loadState()
let store = null

export const storeConfigure = () => {
  const middlewares = [
    ReduxThunk
  ]
  
  if (process.env.NODE_ENV === 'development') {
    const { logger } = require(`redux-logger`)
    const { composeWithDevTools } = require(`redux-devtools-extension`)
    middlewares.push(logger)
    store =  createStore(
      rootReducer,
      persistStore,
      composeWithDevTools(applyMiddleware(...middlewares))
    )
  } else {
    store = createStore(
      rootReducer,
      persistStore,
      applyMiddleware(...middlewares)
    )
  }

  store.subscribe(() => {
    saveState(store.getState())
  })

  // initial store for request

  return store
}

