import { user } from '../constants'

const initialState = null

const userInfoReducers = (state = initialState, action) => {
  switch (action.type) {
    case user.userInfoConstants.SAVE_USERINFO:
      return {
        ...action.payload
      }
    case user.userInfoConstants.CLEAR_USERINFO:
      return null
    default:
      return state
  }
}

export default userInfoReducers