import { authen } from '../constants'

const initialState = {
  accessToken: null,
  refreshToken: null,
  error: null
}

const authReducers = (state = initialState, action) => {
  switch (action.type) {
    case authen.authenConstants.SIGNIN:
    case authen.authenConstants.REFRESH:
      return {
        ...state,
        accessToken: action.payload.accessToken,
        refreshToken: action.payload.refreshToken,
        error: action.payload.error
      }
    case authen.authenConstants.SIGNOUT:
      return initialState
    default:
      return state
  }
}

export default authReducers