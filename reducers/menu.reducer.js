import { menu } from '../constants'

const initialState = []

const menuReducers = (state = initialState, action) => {
  switch (action.type) {
    case menu.menuConstants.SET_MENU:
      return action.payload
    case menu.menuConstants.CLEAR_MENU:
      return initialState
    default:
      return state
  }
}

export default menuReducers