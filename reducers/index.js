import { combineReducers } from 'redux'

import authReducers from './authen.reducer'
import userInfoReducers from './userinfo.reducer'
import menuReducers from './menu.reducer'

const rootReducer = combineReducers({
    auth: authReducers,
    userInfo: userInfoReducers,
    menu: menuReducers
})

export default rootReducer