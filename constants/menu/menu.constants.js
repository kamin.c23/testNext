const BASE_ACTION = 'constants/menu'

export const menuConstants = {
  SET_MENU: `${BASE_ACTION}/set-menu`,
  CLEAR_MENU: `${BASE_ACTION}/clear-menu`,
}