const BASE_ACTION = 'constants/authen'

export const authenConstants = {
  SIGNIN: `${BASE_ACTION}/signin`,
  SIGNOUT: `${BASE_ACTION}/signout`,
  REFRESH: `${BASE_ACTION}/refresh`
}
