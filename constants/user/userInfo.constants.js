const BASE_ACTION = 'constants/userinfo'

export const userInfoConstants = {
  SAVE_USERINFO: `${BASE_ACTION}/save`,
  CLEAR_USERINFO: `${BASE_ACTION}/clear`
}

