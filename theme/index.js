import { colors } from '@material-ui/core';
import { createTheme } from '@material-ui/core/styles';
import shadows from './shadows';
import typography from './typography';

const MyTheme = createTheme({
  palette: {
    background: {
      dark: '#F4F6F8',
      default: colors.common.white,
      paper: colors.common.white,
    },
    primary: {
      main: '#727682',
      // contrastText: colors.common.white
    },
    secondary: {
      main: colors.red['A400'],
    },
    btnSubmit: {
      backgroundColor: colors.indigo[500],
      // color: '#000',
    },
    warning: {
      main: colors.red[500],
      // hover
    },
    text: {
      primary: colors.blueGrey[900],
      secondary: colors.blueGrey[600],
    },
  },
  shadows,
  typography,
});

export default MyTheme;
