const path = require('path')
const rewireReactHotLoader = require('react-app-rewire-hot-loader')

function overrideWebpack(config, env) {
  return {
    ...config,
    output: {
      ...config.output,
      path: path.resolve(__dirname, 'build'),
      publicPath: '/',
      chunkFilename: `[id].${new Date().getTime()}.chunk.js`,
      filename: `main.${new Date().getTime()}.js`
    }
  }
}

module.exports = function override(config, env) {
  config = overrideWebpack(config, env)
  config = rewireReactHotLoader(config, env)
  return config;
}